package com.bunthai.testg.accountservice.api.restcontroller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    private String users = "jonh,jack,steve,ben,clint";

    @GetMapping
    public List<String> users () {
        List<String> userList = Arrays.asList(StringUtils.split(users, ","));
        return  userList;
    }

}
